package fr.inouk.OpenERPJSONRPCClientTests;
/*
Copyright (c) 2013 Cyril MORISSE ( @cmorisse )

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

-- MIT License (MIT)
*/

/**
 * Created with IntelliJ IDEA.
 * User: cmorisse
 * Date: 12/10/13
 * Time: 08:54
 * To change this template use File | Settings | File Templates.
 */
import com.github.kevinsawicki.http.HttpRequest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.*;
import fr.inouk.OpenERPJSONRPCClient.*;

import java.util.ArrayList;


public class CoreTest {
    final String SERVER_URL = "http://localhost";
    final int SERVER_PORT = 8069;
    final String MASTER_ADMIN_PASSWORD = "admin";

    /**
     * java.net.UnknownHostException
     */
    @Test(groups = {"protocol"})
    public void UnknownHost() {
        try {
            OpenERPJSONRPCClient server = new OpenERPJSONRPCClient("http://badserver:" + SERVER_PORT);
        } catch (HttpRequest.HttpRequestException ex) {
            System.out.println("catched HttpRequestException()");
            System.out.println("cause="+ex.getCause());
        }
    }

    /**
     * java.net.ConnectException
     */
    @Test(groups = {"protocol"})
    public void BadPort() {
        try {
            OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL + ":" + 8099);
        } catch (HttpRequest.HttpRequestException ex) {
            System.out.println("catched HttpRequestException()");
            System.out.println("cause="+ex.getCause());
        }
    }

    /**
     * OpenERPJSONRPCCLient.OpenERPServiceOrMethodException
     */
    @Test(groups = {"protocol"})
    public void BadOpenERPServiceOrMethod() {
        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);

        try {
            JSONObject sessionInfo = (JSONObject) server.OEJsonRpc("http://localhost:8069/web/session/get_session_infonas", "call", new JSONObject());
        } catch (OpenERPServiceOrMethodException ex) {
            System.out.println("catched HttpRequestException()");
            System.out.println("cause="+ex.getCause());
        }
    }

    @Test(groups = {"protocol"})
    public void testConnection() {
        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);
        System.out.println("Server="+server);

        JSONObject sessionInfo = (JSONObject) server.OEJsonRpc("http://localhost:8069/web/session/get_session_info", "call", new JSONObject());
        System.out.println("session_info"+ sessionInfo.toString());
    }


    //*****( 'database' service )**************************************************************************************/

    @Test(groups = {"database"}, dependsOnMethods = {"testConnection"})
    public void databaseGetList() {
        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);

        ArrayList<String> dbList = server.databaseGetList(null);
       System.out.println(dbList);
    }

    @Test(groups = {"database"}, dependsOnMethods = {"databaseGetList"})
    public void databaseDrop() {
        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);

        ArrayList<String> dbList = server.databaseGetList(null);
        if( ! dbList.contains("openerp_jsonrpc_client") )
            Assert.fail("Database openerp_jsonrpc_client do not exists !");

        boolean result = server.databaseDrop(MASTER_ADMIN_PASSWORD, "openerp_jsonrpc_client", null);
        System.out.println("databaseDrop() => " + result);
    }

    @Test(groups = {"database"}, dependsOnMethods = {"databaseDrop"}, alwaysRun = true)
    public void databaseCreate() {
        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);

        ArrayList<String> dbList = server.databaseGetList(null);
        if( dbList.contains("openerp_jsonrpc_client") )
            Assert.fail("Database openerp_jsonrpc_client already exists !");

        System.out.println("Starting database creation ! Be patient...");
        boolean installDemoData = true;
        boolean result = server.databaseCreate(MASTER_ADMIN_PASSWORD, "openerp_jsonrpc_client", installDemoData, "fr_FR", "admin", null);
        System.out.println("databaseCreate() => " + result);
    }


    //*****( 'session' service )***************************************************************************************/

    @Test(groups = {"session_service"}, dependsOnMethods = {"databaseCreate"})
    public void sessionAuthenticate() {
        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);

        // Bad database
        try {
            JSONObject sessionInfo = server.sessionAuthenticate("openerp_jsonrpc_clientooooo", "admin", "admin", null, null);
        } catch ( OpenERPJSONRPCClientException exc ) {
           System.out.println(exc);
            Assert.assertEquals("OpenERP Server Error", exc.message, "Authenticate Exception failed");
        }

        // Bad username or password
        try {
            JSONObject sessionInfo = server.sessionAuthenticate("openerp_jsonrpc_client", "admin", "adminisssss", null, null);
            Assert.assertEquals( -1, sessionInfo.getInt("uid"), "Bad Authentication Failure (password) returned value");

            sessionInfo = server.sessionAuthenticate("openerp_jsonrpc_client", "adminisss", "admin", null, null);
            Assert.assertEquals( -1, sessionInfo.getInt("uid"), "Bad Authentication Failure (user) returned value");

        } catch ( OpenERPJSONRPCClientException exc ) {
           System.out.println(exc);
            Assert.assertEquals("OpenERP Server Error", exc.message, "Authenticate Exception failed");
        }

        // Authentication success.
        // User must store sessionInfo as it contains important information
        JSONObject sessionInfo = server.sessionAuthenticate("openerp_jsonrpc_client", "admin", "admin", null, null);
        Assert.assertTrue(sessionInfo instanceof JSONObject, "Login failed");
        System.out.println("login success sessionInfo="+sessionInfo);
    }

    @Test(groups = {"session_service"}, dependsOnMethods = {"sessionAuthenticate"})
    public void sessionGetInfoOnceLogged() {
        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);

        JSONObject sessionInfo = server.sessionGetInfo(null);
        Assert.assertEquals(-1, sessionInfo.get("uid"), "Bad 'No Session' returned value.");

        sessionInfo = server.sessionAuthenticate("openerp_jsonrpc_client", "admin", "admin", null, null);
        Assert.assertEquals(1, sessionInfo.get("uid"), "Admin login failed");

        // We retest GetSessionInfo after login and expect uid = 1 ( 1 = admin user )
        sessionInfo = server.sessionGetInfo(null);
        Assert.assertEquals(1, sessionInfo.get("uid"), "GetSessionInfo() failed as loggedin.");
    }

    //*****( we fill res.users for the search tests )******************************************************************/

    @Test(groups = {"fill_database"}, dependsOnGroups = {"session_service"})
    public void callkw_fill_res_users() {
        /**
         * We use CallKW to create a user using args postional
         */
        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);
        JSONObject sessionInfo = server.sessionAuthenticate("openerp_jsonrpc_client", "admin", "admin", null, null);

        String[] userNames = {
                "Adeline-Candida LISEHIPOLITO",
                "Alma-Anderson MARCELOHARRIETKAYLA",
                "Alphonse CHERISH",
                "Althea-Daisey NATIVIDADLAVONANGELINE",
                "Amy-Roberto MAUDPARTHENIA",
                "Annice-Arianna JAIMEEJACKIEMIYOKO",
                "Arielle ALBERTINAREGGIECONSUELA",
                "Arminda FLOYDSHARRI",
                "Ashli RONNIE",
                "Audie-Alethia SHEMEKA",
        };

        JSONArray args = null;
        JSONObject values = null;

        int i=0;
        while( i < userNames.length ) {
            args = new JSONArray();
            values = new JSONObject();
            values.put("name", userNames[i]);
            values.put("login", "user" + i + "@" + System.currentTimeMillis()); // timestamp for dirty runs
            values.put("new_password", "user" + i );
            args.put(values);
            System.out.print("Created " + values);
            Object response = server.modelCallKW("res.users", "create", args, null, null);
            Assert.assertTrue( response instanceof Integer, "res.users creation Failure");
            System.out.println(" with id = "+ response);
            i++;
        }
    }

    //*****( 'dataset' service )****************************************************************************************
    @Test(groups = {"dataset_service"}, dependsOnGroups = {"fill_database"})
    public void searchRead_Naive() {
        final String[] fieldNames = {"id", "name", "login"};

        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);
        JSONObject sessionInfo = server.sessionAuthenticate("openerp_jsonrpc_client", "admin", "admin", null, null);

        // res.users, 3 fields, all objects(no domain), no sort, no context
        // Note that we are supposed to have 40 + 1 users in database
        JSONObject objects = server.modelSearchRead("res.users", fieldNames, 0, 0, null, null, null);
        Integer userCount = (Integer) objects.get("length");
        Assert.assertTrue(userCount > 10, "naive search_read() failed. Are you sure you fill res_users with CallKW_fill_res_partner()");

    }

    @Test(groups = {"dataset_service"}, dependsOnMethods = {"searchRead_Naive"})
    public void searchRead_Sort() {
        final String[] fieldNames = {"id", "name", "login"};

        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);
        JSONObject sessionInfo = server.sessionAuthenticate("openerp_jsonrpc_client", "admin", "admin", null, null);

        // Test sort parameters
        JSONObject sortedObjects = server.modelSearchRead("res.users", fieldNames, 0, 0, null, "id", null);
        Integer sortedObjectCount = (Integer) sortedObjects.get("length");
        Integer idOf5th = (Integer) ((JSONObject)((JSONArray)sortedObjects.get("records")).get(5)).get("id");
        Integer idOf6th = (Integer) ((JSONObject)((JSONArray)sortedObjects.get("records")).get(6)).get("id");
        Assert.assertTrue( idOf5th == idOf6th - 1, "Humm search_read() with sort failed.");

        // sort 'DESC'
        sortedObjects = server.modelSearchRead("res.users", fieldNames, 0, 0, null, "id desc", null);
        sortedObjectCount = (Integer) sortedObjects.get("length");
        idOf5th = (Integer) ((JSONObject)((JSONArray)sortedObjects.get("records")).get(5)).get("id");
        idOf6th = (Integer) ((JSONObject)((JSONArray)sortedObjects.get("records")).get(6)).get("id");
        Assert.assertTrue( idOf5th == idOf6th + 1, "Humm search_read() sort failed.");
        System.out.println(sortedObjectCount + "sortedObjects:");
        System.out.println(sortedObjects);

    }

    @Test(groups = {"dataset_service"}, dependsOnMethods = {"searchRead_Sort"})
    public void searchRead_Domain() {
        final String[] fieldNames = {"id", "name", "login"};

        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);
        JSONObject sessionInfo = server.sessionAuthenticate("openerp_jsonrpc_client", "admin", "admin", null, null);

        // Test domain parameter
        JSONArray domain_expr = new JSONArray("[['id','>',5],['id','<',10]]");
        JSONObject filteredObjects = server.modelSearchRead("res.users", fieldNames, 0, 0, domain_expr, "id", null);
        Integer filteredObjectCount = (Integer) filteredObjects.get("length");
        Assert.assertTrue( filteredObjectCount == 4, "Humm search_read() with a domain failed !");
        System.out.println("We got " + filteredObjectCount + " filteredObjects:");
        System.out.println(filteredObjects);

    }

    @Test(groups = {"dataset_service"}, dependsOnMethods = {"searchRead_Domain"})
    public void searchRead_Paginated() {
        final String[] fieldNames = {"id", "name", "login"};

        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);
        JSONObject sessionInfo = server.sessionAuthenticate("openerp_jsonrpc_client", "admin", "admin", null, null);
        /*
         * Warning: with limit and offset, result.length still contains total number of records returned by the search.
         * But result.records contains only limit record.
         * Example with limit=2 offset=10
            {
                "id": "r2",
                "jsonrpc": "2.0",
                "result": {
                    "length": 562,
                    "records": [
                        {
                            "id": 30,
                            "login": "user3_1382175375491",
                            "name": "Althea-Daisey NATIVIDADLAVONANGELINE"
                        },
                        {
                            "id": 31,
                                "login": "user4_1382175375558",
                                "name": "Amy-Roberto MAUDPARTHENIA"
                        }
                    ]
                }
            }
         */

        /*
         * To test offset and limit we search to overlapping pages and we check that the first id of the second page equals
         * the last id of the first page.
         */

        // First page: offset = 0 and limit = 2 (so response is printable)
        JSONObject paginatedObjects = server.modelSearchRead("res.users", fieldNames, 0, 2, null, "id", null);

        // We test limit
        Integer paginatedObjectCount = ((JSONArray)paginatedObjects.get("records")).length();
        System.out.println("We got "+paginatedObjectCount + " paginatedObjects:");
        System.out.println(paginatedObjects);
        Assert.assertEquals( paginatedObjectCount.intValue(), 2, "Humm search_read() with limit and offset failed on the limit).");

        int idOfLastObjectOfPage1 = ((Integer)((JSONObject)((JSONArray)paginatedObjects.get("records")).get(1)).get("id")).intValue();

        // Second page: offset = 1 and limit = 2
        JSONObject page2Objects = server.modelSearchRead("res.users", fieldNames, 1, 2, null, "id", null);
        int idOfFirstObjectOfPage2 = ((Integer)((JSONObject)((JSONArray)page2Objects.get("records")).get(0)).get("id")).intValue();

        // We test offset
        Assert.assertEquals( idOfLastObjectOfPage1, idOfFirstObjectOfPage2, "Humm search_read() with limit and offset failed (offset).");

    }


    @Test(groups = {"dataset_service"}, dependsOnMethods = {"searchRead_Paginated"})
    public void modelLoad() {
        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);
        JSONObject sessionInfo = server.sessionAuthenticate("openerp_jsonrpc_client", "admin", "admin", null, null);
        // res.users, all fields, all objects(no domain), no sort, no context
        JSONObject object = server.modelLoad("res.users", 1 , null);
        System.out.println(object);
    }

    @Test(groups = {"dataset_service_callkw"}, dependsOnGroups = {"dataset_service"})
    public void callkw_create_using_kwargs() {
        /**
         * We use CallKW to create a user using kwargs (keyword arguments)
         */
        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);
        JSONObject sessionInfo = server.sessionAuthenticate("openerp_jsonrpc_client", "admin", "admin", null, null);

        JSONObject kwargs = new JSONObject();

        JSONObject values = new JSONObject();
        values.put("name", "Cyril MORISSE");
        values.put("login", "cmorisse@" + System.currentTimeMillis());
        values.put("new_password", "cmorisse");
        kwargs.put("values", values);

        Object response = server.modelCallKW("res.users", "create", null, kwargs, null);
        // We expect the id of the created user
        Assert.assertTrue( response instanceof Integer, "res.users creation Failure");
        System.out.println("res.users.create() => "+response);

    }

    @Test(groups = {"dataset_service_callkw"}, dependsOnGroups = {"dataset_service"})
    public void callkw_create_using_args() {
        /**
         * We use CallKW to create a user using args (positional arguments)
         */
        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);
        JSONObject sessionInfo = server.sessionAuthenticate("openerp_jsonrpc_client", "admin", "admin", null, null);

        JSONArray args = new JSONArray();

        JSONObject values = new JSONObject();
        values.put("name", "Cyril MORISSE");
        values.put("login", "cmorisse@" + System.currentTimeMillis());
        values.put("new_password", "cmorisse");
        args.put(values);

        Object response = server.modelCallKW("res.users", "create", args, null, null);
        // We expect the id of the created user
        Assert.assertTrue( response instanceof Integer, "res.users creation Failure");
        System.out.println("res.users.create() => "+response);

    }

    @Test(groups = {"dataset_service_workflow"}, dependsOnGroups = "dataset_service_callkw")
    public void installSaleModule() {
        System.out.println("Installing Sale module (Remember database has been created with demo data).");

        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);
        JSONObject sessionInfo = server.sessionAuthenticate("openerp_jsonrpc_client", "admin", "admin", null, null);

        // First we search for sale module id
        JSONArray domain = new JSONArray("[['name','=','sale']]");
        String[] fields = { "name", "state"};
        JSONObject jsonResult = server.modelSearchRead("ir.module.module", fields, 0, 0, domain, null, null);
        JSONObject moduleObject = (JSONObject) ((JSONArray) jsonResult.get("records")).get(0);
        Long saleModuleId = ((Integer)moduleObject.get("id")).longValue();
        System.out.println("Sale module (before install)= " + moduleObject);
        Assert.assertEquals("uninstalled", (String) moduleObject.get("state"), "Sale module is already installed");

        // Second we install it and check new state is installed
        JSONArray args = new JSONArray("[[" + saleModuleId + "]]");  // Notice that the argument we sent is an array !!
        Object objResult = server.modelCallKW("ir.module.module", "button_immediate_install", args, null, null);
        // Server returns us an ir.actions to open a menu. We can't process it.
        System.out.println("objResult = "+objResult);

        jsonResult = server.modelSearchRead("ir.module.module", fields, 0, 0, domain, null, null);
        moduleObject = (JSONObject) ((JSONArray) jsonResult.get("records")).get(0);
        System.out.println("Sale module (after install)= " + moduleObject);
        Assert.assertEquals("installed", (String) moduleObject.get("state"), "Sale module installation failed");
    }

    @Test(groups = {"dataset_service_workflow"}, dependsOnMethods = "installSaleModule" )
    public void modelExecWorkflow() {
        /*
         * exec_workflow is heavy to test.
         * But since we have just installed Sale module on a fresh database, we can:
         *   -confirm sale_order 1
         *   -check her state changed to 'manual'
         */
        OpenERPJSONRPCClient server = new OpenERPJSONRPCClient(SERVER_URL+":"+SERVER_PORT);
        JSONObject sessionInfo = server.sessionAuthenticate("openerp_jsonrpc_client", "admin", "admin", null, null);

        // We send the signal "order_confirm" to the sale.order workflow on object 1
        Object oResult = server.modelExecWorkflow("sale.order", 1, "order_confirm", null);
        System.out.println("oResult = " + oResult);

        // We check sale order state
        JSONObject orderObject = server.modelLoad("sale.order", 1, null);
        String orderObjectState = (String) ((JSONObject)orderObject.get("value")).get("state");
        System.out.println("Sale Order state = " + orderObjectState);
        Assert.assertEquals("manual", orderObjectState, "Exec Wrokflow failed.");
    }
}